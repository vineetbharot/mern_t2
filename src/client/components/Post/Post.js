import React from 'react';

import moment from 'moment';

import './Post.css';

const post = props => (
  <article className="Post" onClick={props.clicked}>
    <h1>{props.name}</h1>
    <h1>{moment(props.dob).format('YYYY-MM-DD')}</h1>
    {/* <div className="Info">
      <div className="Author">{props.author}</div>
    </div> */}
    <div className="Edit">
      <button onClick={props.deleted} className="Delete">Delete</button>
    </div>
  </article>
);

export default post;
