import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

import './NewPost.css';

class NewPost extends Component {
    state = {
        name: '',
        dob: '',
        salary: '',
        submitted: false
    }

    componentDidMount () {
        // If unauth => this.props.history.replace('/posts');
        console.log( this.props );
    }

    postDataHandler = () => {
        const data = {
            name: this.state.name,
            dob: this.state.dob,
            salary: this.state.salary,
            skills: []
        };
        axios.post( '/employee', data )
            .then( response => {
                console.log( response );
                this.props.history.replace('/posts');
                // this.setState( { submitted: true } );
            } );
    }

    render () {
        let redirect = null;
        if (this.state.submitted) {
            redirect = <Redirect to="/posts" />;
        }
        return (
            <div className="NewPost">
                {redirect}
                <h1>Add a Employee</h1>
                <label>Name</label>
                <input type="text" value={this.state.name} onChange={( event ) => this.setState( { name: event.target.value } )} />
                <label>Date of Birth in format "YY-MM-DD"</label>
                <input type="text" value={this.state.dob} onChange={( event ) => this.setState( { dob: event.target.value } )} />
                <label>salary</label>
                <input type="text" value={this.state.salary} onChange={( event ) => this.setState( { salary: event.target.value } )} />
                <button onClick={this.postDataHandler}>Add Post</button>
            </div>
        );
    }
}

export default NewPost;