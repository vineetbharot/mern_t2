// eslint-disable-next-line max-len
// borrowed from here: http://stackoverflow.com/questions/5778245/expressjs-how-to-structure-an-application/7350875#7350875

// eslint-disable-next-line no-undef
const dotenv = require('dotenv');

dotenv.load();
let currentEnv;

if (process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'test') {
  currentEnv = process.env.NODE_ENV;
} else {
  currentEnv = 'production';
}

const appName = 'mern_t1';

const dbName = `${appName.toLowerCase()}_${currentEnv}`;

const db = {
  URL: process.env.MONGODB_URI
    || `mongodb://localhost:27017/${dbName}`,
  name: dbName,
};

const secret = process.env.SESSION_SECRET || 'testSecret';

module.exports = {
  currentEnv,
  appName,
  db,
  secret
};
