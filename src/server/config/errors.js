const HTTPStatus = require('http-status');

const missingParam = (params) => {
  const missingParamsError = new Error(`invalid or missing argument ${params}`);
  missingParamsError.statusCode = HTTPStatus.BAD_REQUEST;
  return missingParamsError;
};

const duplicateParam = (paramName, paramValue) => {
  const duplicateParamError = new Error(`${paramName} ${paramValue} is already taken. Please provide another one!`);
  duplicateParamError.statusCode = HTTPStatus.UNPROCESSABLE_ENTITY;
  return duplicateParamError;
};

const invalidActionType = (actionType) => {
  const invalidTypeError = new Error(`please pass valid actionType, got: ${actionType}`);
  invalidTypeError.statusCode = HTTPStatus.BAD_REQUEST;
  return invalidTypeError;
};

const invalidCredentials = (params) => {
  const invalidCredentialsErr = new Error(`Your login ${params} could not be verified. Please try again.`);
  invalidCredentialsErr.statusCode = HTTPStatus.BAD_REQUEST;
  return invalidCredentialsErr;
};

const unauthorized = () => {
  const unauthorizedErr = new Error('unauthorized request');
  unauthorizedErr.statusCode = HTTPStatus.UNAUTHORIZED;
  return unauthorizedErr;
}

module.exports = {
  missingParam,
  invalidActionType,
  invalidCredentials,
  duplicateParam,
  unauthorized,
};