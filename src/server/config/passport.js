const LocalStrategy = require('passport-local');
const passport = require('passport');
const appSecret = require('../config/config').secret;
const helper = require('../../utils/helper');
const errors = require('../config/errors');
const User = require('../models/User');

const localOptions = {
  usernameField: 'email',
  passwordField: 'password'
};

const localLogin = new LocalStrategy(
  localOptions,
  ((email, password, done) => {
    User.findOne({ email }, (err, user) => {
      console.log(`localLOgin ${email} ${user}`);
      console.log('done', done);
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false, { error: 'Your login details could not be verified. Please try again.' });
      }

      user.comparePassword(password, (err, isMatch) => {
        if (err) {
          return done(err);
        }
        if (!isMatch) {
          return done(null, false, { error: 'Your login details could not be verified. Please try again.' });
        }

        return done(null, user);
      });
    });
  })
);

// used to serialize the user for the session
passport.serializeUser((user, done) => {
  done(null, user._id);
});

// used to deserialize the user
passport.deserializeUser((_id, done) => {
  User.findById({ _id }, (err, user) => {
    done(err, user);
  });
});

passport.use(localLogin);
