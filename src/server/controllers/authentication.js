const User = require('../models/User');
const helper = require('../../utils/helper');
const errors = require('../config/errors');

const register = async (req, res, next) => {
  try {
    const {
      email,
      userName,
      password
    } = req.body;
    if (!email) {
      throw errors.missingParam('email');
    } else if (!userName) {
      throw errors.missingParam('userName');
    } else if (!password) {
      throw errors.missingParam('password');
    }

    const existingUser = await User.findOne({ email });
    if (existingUser) {
      throw errors.duplicateParam('email', email);
    }

    const newUser = new User({
      email,
      password,
      userName
    });

    const savedUser = await newUser.save();

    return helper.handleResponse(req, res, null, savedUser);
  } catch (error) {
    return helper.handleResponse(req, res, error);
  }
};

const login = async (req, res, next) => {
  try {
    console.log(`user: ${req.user._id} ${req.user.userName} logged in succesfully`);
    return helper.handleResponse(req, res, null, true);
  } catch (error) {
    return helper.handleResponse(req, res, error);s
  }
};

module.exports = {
  register,
  login
};