
const express = require('express');
const os = require('os');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const User = require('./models/User');
const { currentEnv, db } = require('./config/config');
const passportConfig = require('./config/passport');

const app = express();

app.use(bodyParser.json({ limit: '500mb' }));
app.use(bodyParser.raw({ limit: '500mb' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '500mb' }));
app.use(express.static('dist'));

mongoose.Promise = require('bluebird');

mongoose.connect(db.URL);

mongoose.connection.on('connected', async () => {
  try {
    console.log(`connected to ${db.URL}`);
    const user = new User({
      email: 'test_1@mailinator.com',
      password: '1234567890',
      userName: 'test'
    });
    const saveRes = await user.save();
    console.log(`user saveRes ${saveRes}`);
  } catch (error) {
    console.error(`connected action error ${error.message}`);
  }
});

mongoose.connection.on('error', (error) => {
  console.log(`MongoDB Connections error ${error}`);
  process.exit(1);
});

app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET || 'adsf0234lkasjdf23afd',
  store: new MongoStore({
    url: db.URL,
    autoReconnect: true
  })
}));

app.use(passport.initialize());
app.use(passport.session());

app.use('/', require('./routes'));

// error handler
app.use((err, req, res, next) => {
  console.trace('END err', err);
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render('error');
  res.json(err);
});


app.listen(8080, () => console.log('Listening on port 8080!'));
