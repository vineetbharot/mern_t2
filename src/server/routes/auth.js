const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportConfig = require('../config/passport');
const {
  authentication
} = require('../controllers');


const requireLogin = passport.authenticate('local', { session: true });

router.post('/register', authentication.register);
router.post('/login', requireLogin, authentication.login);

module.exports = router;