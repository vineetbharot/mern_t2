const express = require('express');

const errors = require('../config/errors');
const helper = require('../../utils/helper');

const router = express.Router();

router.use('/auth', require('./auth'));

router.use('/*', async (req, res, next) => {
  console.log('auth middleware', req.user, req.isAuthenticated(), req.session);
  if (req.isAuthenticated()) {
    return next();
  }
  return helper.handleResponse(req, res, errors.unauthorized());
});

router.use('/*', (req, res, next) => {
  console.log('req.path', req.path, 'req.session', req.session, req.isAuthenticated());
  return helper.handleResponse(req, res, null, req.user);
});

module.exports = router;