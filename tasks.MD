- [ ] TAKE APPROVAL AFTER DESIGNING DATABASE
- [ ] discuss assumptions
- [ ] handle errors all

data
- [ ] mongo db collections users info saved
- [ ] users collection
    - [ ] user name
    - [ ] points received for free
    - [ ] points purchased
    - [ ] items bought by spending points

- [ ] transactions
    - [ ] timestamp
    - [ ] amount
    - [ ] points purchased
    - [ ] points received for free
    - [ ] items purchased with points

- [ ] items avaiable to purchase

microservice docker deployment

application
- [ ] user given 50 points free every 3 hours irrespective of user connected to system or not
- [ ] points accumulate in account
- [ ] upper limit 200 points and no more free until user spends/consumes few(?q1) of them.
- [ ] countdown only starts when player has less that 200 in account
- [ ] FREE POINTS TRANSACTIONS TO BE RECORED FOR FUTURE VERIFICATION.

- [ ] user can buy points as well
- [ ] KEEP TRACK OF FREE AND PAID POINTS SEPERATELY
- [ ] COS FREE SHOULD BE SPEND FIRST THEN PURCHASED ONE

---
endpoints
- [ ] connect (to server),
    - [ ] use a secure method to login.
    - [ ] response users
        - [ ] inventory
        - [ ] points (free and purchased)

- [ ] getPoints
    - [ ] returns free and purchased points

- [ ] getItems
    - [ ] returns items purchased by user

- [ ] purchaseItem
    - [ ] user buy and item which is present in backend inventory

- [ ] getInventory
    - [ ] items available to purchase by users

---
- [ ] discuss SECURE METHOD TO LOGIN.
- [ ] q1. how many is few (ASSUMING ANYTHINS LESS THEN 200 THEN JUST ADD TO MAKE IT 200 TOTAL).
- [ ] BUY POINTS WITH MONEY TRANSACTIONS NEED TO BE MAINTAINED SEPERATELY.(assumed now)
- [ ] CASE IF USER BUTY 200 THEN ALSO STOP GIVING FREE LIKE POINT COUNT ON TOTAL OR ON FREE ONLY


---
- [ ] ask questions to first before starting 
- [ ] ADVICES DON'T OVERCOMPLICATE

---
deployment
- [ ] AWS free tier account


---
PROBLEMS
1. add authentication
2. add cron to credit points
3. deploy how
4. points strucutre.